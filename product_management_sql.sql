create database Product_Management4;
use Product_Management4;
show tables;
CREATE TABLE Product(
Name varchar(500),
Quantity varchar(70),
Price varchar(50)
);
CREATE TABLE logindata(
email varchar(500),
pass varchar(700)
);
drop table Product;
drop table logindata;

Insert into Product(Name,Quantity,Price) values ('pen','10','20');
Insert into Product(Name,Quantity,Price) values ('Book','10','60');
Insert into logindata(email,pass)values('sagar','888');
Insert into logindata(email,pass)values('sanket','8862');
Insert into logindata(email,pass)values('gaurav','788');
Insert into logindata(email,pass)values('ram','8898');

select * from Product;
select*from logindata;