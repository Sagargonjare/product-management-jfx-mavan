package addProducts;

import java.sql.Connection;
import java.sql.Statement;

import commonScreen.CommonScreen;
import dbOperations.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import showOptions.ShowOptions;



public class AddProductController  {
	static Statement stmt;
	static Connection con;
	@FXML
	private TextField productName;
	@FXML
	private TextField producQuantity;
	@FXML
	private TextField productPrice;
	@FXML
	private Button back;
	@FXML
	private Button add;

	public void add(ActionEvent event) {
		System.out.println(productName.getText());
		System.out.println(producQuantity.getText());
		System.out.println(productPrice.getText());
		String query = "insert into Product(Name,Quantity,Price) values ('" + productName.getText() + "', '" + producQuantity.getText() + "','"
				+ productPrice.getText() + "');";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur add controller "+event.getEventType().getName());
		new ShowOptions().show();

	}

}